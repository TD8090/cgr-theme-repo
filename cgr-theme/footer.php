<?php
/**
 * @package WordPress
 * @subpackage cgr-theme
 */
?>
<?php
// For the SEO purposes the preheader is placed here
//get_template_part( 'preheader' );
?>

        </div> <!--end:#main -->

        <div id="cgrfooter">
        	<?php wp_footer(); ?>

            <?php if (ot_get_option( 'copyright' )!=''): ?>
                <div id="copyright"><?php echo ot_get_option( 'copyright' ) ?></div>
            <?php endif; ?>
        </div>

</div> <!-- end: wrapper -->
    </body>
</html>
