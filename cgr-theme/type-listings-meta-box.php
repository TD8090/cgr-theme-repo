<?php


// Add the Meta Box
function add_lstng_meta_box() {
    add_meta_box(
		'lstng_meta_box', // $id
		'Listings Meta Box', // $title
		'show_lstng_meta_box', // $callback
		'listings', // $page
		'normal', // $context
		'high'); // $priority
}
add_action('add_meta_boxes', 'add_lstng_meta_box');

// Field Array
$prefix = 'cgr_lstng_';
$lstng_meta_fields = array(
    array(
        'label'	=> __('Coming Soon, For Sale, or Recently Sold','cgrslug'),
        'desc'	=> __('','cgrslug'),
        'id'	=> $prefix.'soon-sale-sold',
        'type'	=> 'radio',
        'options' => array(
            ['value'=>'comsoon', 'label'=>'Coming Soon'],
            ['value'=>'forsale', 'label'=>'For Sale'],
            ['value'=>'recsold', 'label'=>'Recently Sold']
        )
    ),
    array(
        'label'	=> __('Town, City','cgrslug'),
        'desc'	=> __('Title of the Listing','cgrslug'),
        'id'	=> $prefix.'title',
        'type'	=> 'text'
    ),
      array(
        'label'	=> __('Tagline','cgrslug'),
        'desc'	=> __('Format: 2BR | 2BA','cgrslug'),
        'id'	=> $prefix.'tagline',
        'type'	=> 'text'
    ),

    array(
        'label'	=> __('Price','cgrslug'),
        'desc'	=> __('Price or Price Range','cgrslug'),
        'id'	=> $prefix.'price',
        'type'	=> 'text'
    ),
    array(
        'label'	=> __('Square Footage','cgrslug'),
        'desc'	=> __('Total living space','cgrslug'),
        'id'	=> $prefix.'sqrfeet',
        'type'	=> 'text'
	),
	array(
        'label'	=> __('Bedrooms and Bathrooms','cgrslug'),
        'desc'	=> __('Format: "2BR, 2BA" or "2br | 2ba", etc','cgrslug'),
        'id'	=> $prefix.'num-bedbath',
        'type'	=> 'text'
	),
	array(
        'label'	=> __('# Bedrooms','cgrslug'),
        'desc'	=> __('','cgrslug'),
        'id'	=> $prefix.'class',
        'type'	=> 'select',
        'options' => array(
            ['value'=>'1', 'label'=>'1BR'],
            ['value'=>'2', 'label'=>'2BR'],
            ['value'=>'3', 'label'=>'3BR'],
            ['value'=>'4', 'label'=>'4BR']
        )
	),
	array(
        'label'	=> __('# Bathrooms','cgrslug'),
        'desc'	=> __('','cgrslug'),
        'id'	=> $prefix.'num-bathrooms',
        'type'	=> 'select',
        'options' => array(
            ['value'=>'1', 'label'=>'1 ba'],
            ['value'=>'1.5', 'label'=>'1.5 ba'],
            ['value'=>'2', 'label'=>'2 ba'],
            ['value'=>'2.5', 'label'=>'2.5 ba'],
            ['value'=>'3', 'label'=>'3 ba'],
            ['value'=>'3.5', 'label'=>'3.5 ba'],
            ['value'=>'4', 'label'=>'4 ba'],
            ['value'=>'4.5', 'label'=>'4.5 ba']
        )
	),
/*    array(
        'label'	=> __('Amenities','cgrslug'),
        'desc'	=> __('Amenities or Features','cgrslug'),
        'id'	=> $prefix.'amenities',
        'type'	=> 'excerpt'
    ),*/
    array(
        'label'	=> __('External Link','cgrslug'),
		'desc'	=> __('External URL to include if desired','cgrslug'),
		'id'	=> $prefix.'ext_link',
		'type'	=> 'text'
	),
	array(
		'label'	=> __('Image 1','cgrslug'),
		'desc'	=> __('Image #1','cgrslug'),
		'id'	=> $prefix.'img1',
		'type'	=> 'image'
	)

);

// enqueue scripts and styles, but only if is_admin

function cgr_add_admin_meta_box(){
	wp_enqueue_script('lstng-meta-box', get_template_directory_uri().'/src/js/lstng-meta-box.js','jquery');
}
if( is_admin() ) {
	add_action('admin_init', 'cgr_add_admin_meta_box');
}


// The Callback
function show_lstng_meta_box() {
	global $lstng_meta_fields, $post;
	// Use nonce for verification
	echo '<input type="hidden" name="lstng_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	
	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($lstng_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td>';
				switch($field['type']) {
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// textarea
					case 'textarea':
						echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="30" rows="4">'.$meta.'</textarea>
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox
					case 'checkbox':
						echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>
								<label for="'.$field['id'].'">'.$field['desc'].'</label>';
					break;
					// select
					case 'select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
						foreach ($field['options'] as $option) {
							echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// radio
					case 'radio':
						foreach ( $field['options'] as $option ) {
							echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$meta == $option['value'] ? ' checked="checked"' : '',' />
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// checkbox_group
					case 'checkbox_group':
						foreach ($field['options'] as $option) {
							echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"',$meta && in_array($option['value'], $meta) ? ' checked="checked"' : '',' /> 
									<label for="'.$option['value'].'">'.$option['label'].'</label><br />';
						}
						echo '<span class="description">'.$field['desc'].'</span>';
					break;
					// tax_select
					case 'tax_select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
						$terms = get_terms($field['id'], 'get=all');
						$selected = wp_get_object_terms($post->ID, $field['id']);
						foreach ($terms as $term) {
							if (!empty($selected) && !strcmp($term->slug, $selected[0]->slug)) 
								echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>'; 
							else
								echo '<option value="'.$term->slug.'">'.$term->name.'</option>'; 
						}
						$taxonomy = get_taxonomy($field['id']);
						echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">Manage '.$taxonomy->label.'</a></span>';
					break;
					// post_list
					case 'post_list':
					$items = get_posts( array (
						'post_type'	=> $field['post_type'],
						'posts_per_page' => -1
					));
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
								<option value="">Select One</option>'; // Select One
							foreach($items as $item) {
								echo '<option value="'.$item->ID.'"',$meta == $item->ID ? ' selected="selected"' : '','>'.$item->post_type.': '.$item->post_title.'</option>';
							} // end foreach
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
					// date
					case 'date':
						echo '<input type="text" class="datepicker" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// slider
					case 'slider':
					$value = $meta != '' ? $meta : '0';
						echo '<div id="'.$field['id'].'-slider"></div>
								<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" size="5" />
								<br /><span class="description">'.$field['desc'].'</span>';
					break;

					// file
					case 'file':
						$image = get_template_directory_uri().'/src/img/pdf.png';
						echo '<img src="'.$image.'" class="custom_preview_image" alt="" />
								<input name="'.$field['id'].'" type="hidden" class="custom_upload_file" value="'.$meta.'" />
								<p>URL: <a target="_blank" href="'.$meta.'">'.$meta.'</a></p><br />
								<input class="custom_upload_file_button button" type="button" value="Choose file" />
								<small>&nbsp;<a href="#" class="custom_clear_file_button">'.__('Remove file','cgrslug').'</a></small>
								<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// image
					case 'image':
						$image = get_template_directory_uri().'/src/img/image.png';
						echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium');	$image = $image[0]; }				
						echo	'<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
									<img src="'.$image.'" class="custom_preview_image" alt="" /><br />
										<input class="custom_upload_image_button button" type="button" value="Choose Image" />
										<small>&nbsp;<a href="#" class="custom_clear_image_button">'.__('Remove image','cgrslug').'</a></small>
										<br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
					// repeatable
					case 'repeatable':
						echo '<a class="repeatable-add button" href="#">+</a>
								<ul id="'.$field['id'].'-repeatable" class="custom_repeatable">';
						$i = 0;
						if ($meta) {
							foreach($meta as $row) {
								echo '<li><span class="sort hndle">|||</span>
											<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="'.$row.'" size="30" />
											<a class="repeatable-remove button" href="#">-</a></li>';
								$i++;
							}
						} else {
							echo '<li><span class="sort hndle">|||</span>
										<input type="text" name="'.$field['id'].'['.$i.']" id="'.$field['id'].'" value="" size="30" />
										<a class="repeatable-remove button" href="#">-</a></li>';
						}
						echo '</ul>
							<span class="description">'.$field['desc'].'</span>';
					break;
				} //end switch
		echo '</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}

 function remove_taxonomy_boxes() {
 	//remove_meta_box('categorydiv', 'post', 'side');
 }
 add_action( 'admin_menu' , 'remove_taxonomy_boxes' );

// Save the Data
function save_lstng_meta($post_id) {
    global $lstng_meta_fields;
	
	
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if (array_key_exists('post_type', $_POST)){
		if ('page' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id))
				return $post_id;
			} elseif (!current_user_can('edit_post', $post_id)) {
				return $post_id;
		}
	}
		
	// echo "<pre>"; var_dump($_POST);echo"</pre>";
	// die();
	// loop through fields and save the data
	foreach ($lstng_meta_fields as $field) {
		if($field['type'] == 'tax_select') continue;
		$old = get_post_meta($post_id, $field['id'], true);
		$new = '';
		if ( array_key_exists($field['id'], $_POST)){

			$new = $_POST[$field['id']];

			if ($new && $new != $old) {
				update_post_meta($post_id, $field['id'], $new);
			} elseif ('' == $new && $old) {
				delete_post_meta($post_id, $field['id'], $old);
			}
		}

			
	} // enf foreach

	// save taxonomies
	$post = get_post($post_id);
	if ( array_key_exists('category', $_POST)){
		$category = $_POST['category'];
		wp_set_object_terms( $post_id, $category, 'category' );
	}
	
}
add_action('save_post', 'save_lstng_meta');