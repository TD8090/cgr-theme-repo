

<?php get_header(); ?>

	<div id="single-listings-php" class="cgr-page home" data-pos="home" data-url="<?php the_permalink(); ?>">
		<div id="inside">



		    <div class="page-container" id="ajax-single-post">

				<!-- here will be populated with the single post content -->
				<?php the_post(); ?>
				<div class="pageheadsect">
				    <div class="headsectcontent">
				        <div class="section-container">
				            
				            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
							<?php if($url): ?>
								<img src="<?php echo $url; ?>" alt="" class="img-responsive"/>
							<?php endif; ?>
				            <h2 class="title"><?php the_title(); ?></h2>
				            
				            <div class="post-meta">
				            	<!--removed blog meta-->


				            </div>
				            
				        </div>
				    </div>
				</div>

				<div class="page-contents col-md-8 color-1">
					<div class="section">
						<div class="section-container">
							<?php the_content(); ?>
							<?php wp_link_pages(); ?>
						</div>
					</div>
				</div>
				<div class="sidebar-contents col-md-4 color-2">
					<div class="section">
						<div class="section-container">
							<?php the_content(); ?>
							<?php wp_link_pages(); ?>
						</div>
					</div>
				</div>


			</div>


			</div>

		</div>
	</div>

	<?php get_footer(); ?>



