<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passed to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'headsect'       => 'test'
    ),
    'sections'        => array( 
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'topbar',
        'title'       => 'TopBar'
      ),
//      array(
//        'id'          => 'fa_colors',
//        'title'       => 'Theme Colors'
//      ),
//      array(
//        'id'          => 'typography',
//        'title'       => 'Typography'
//      ),
      array(
        'id'          => 'blog',
        'title'       => 'Blog'
      ),
      array(
        'id'          => 'listings',
        'title'       => 'Listings'
      ),
      array(
        'id'          => 'etc',
        'title'       => 'Misc'
      )
    ),

    /* ^^^^ SECTIONS ^^^^
    /  vvvv SETTINGS vvvv */

    'settings'        => array( 
      array(
        'id'          => 'person_name',
        'label'       => 'Your Name',
        'desc'        => 'Type in your complete name here',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'sub_title',
        'label'       => 'Sub-title',
        'desc'        => 'A sub text just below your name, can be your university name, etc',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      //new option brand_logo
      array(
        'id'          => 'brand_logo',
        'label'       => 'Brand logo',
        'desc'        => 'Upload your brand logo here.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'personal_photo',
        'label'       => 'Personal photo',
        'desc'        => 'Upload your personal photo here. It will be shown at the top of the header.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),

        /*START: TopBar settings */
      array(
          'id'          => 'topbar_phone_tog',
          'label'       => 'Phone Number',
          'desc'        => '',
          'std'         => 'off',
          'type'        => 'on-off',
          'section'     => 'topbar',
          'rows'        => '',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => '',
          'operator'    => 'and'
      ),
      array(
          'id'          => 'topbar_phone_number',
          'label'       => 'Phone Number',
          'desc'        => '',
          'std'         => '',
          'type'        => 'text',
          'section'     => 'topbar',
          'rows'        => '',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'topbar_phone_tog:is(on)',
          'operator'    => 'and'
      ),
      array(
          'id'          => 'topbar_email_tog',
          'label'       => 'Email',
          'desc'        => '',
          'std'         => 'off',
          'type'        => 'on-off',
          'section'     => 'topbar',
          'rows'        => '',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => '',
          'operator'    => 'and'
      ),
      array(
          'id'          => 'topbar_email_address',
          'label'       => 'Email address',
          'desc'        => '',
          'std'         => '',
          'type'        => 'text',
          'section'     => 'topbar',
          'rows'        => '',
          'post_type'   => '',
          'taxonomy'    => '',
          'min_max_step'=> '',
          'class'       => '',
          'condition'   => 'topbar_email_tog:is(on)',
          'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_facebook_tog',
        'label'       => 'Facebook',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_facebook_url',
        'label'       => 'Facebook url',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_facebook_tog:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_twitter_tog',
        'label'       => 'Twitter',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_twitter_url',
        'label'       => 'Twitter URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_twitter_tog:is(on)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_linkedin_tog',
        'label'       => 'LinkedIn',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_linkedin_url',
        'label'       => 'LinkedIn URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_linkedin_tog:is(on)',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'topbar_gplus_tog',
        'label'       => 'Google+',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_gplus_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_gplus_tog:is(on)',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'topbar_youtube_tog',
        'label'       => 'YouTube',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_youtube_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_youtube_tog:is(on)',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'topbar_instagram_tog',
        'label'       => 'Instagram',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_instagram_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_instagram_tog:is(on)',
        'operator'    => 'and'
      ),

      array(
        'id'          => 'topbar_flickr_tog',
        'label'       => 'Flickr',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'topbar_flickr_url',
        'label'       => 'URL',
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'topbar',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'topbar_flickr_tog:is(on)',
        'operator'    => 'and'
      ),



/* START: Blog Settings */
      array(
        'id'          => 'blog_ajax',
        'label'       => 'Use Ajax for blog pages?',
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_twitter',
        'label'       => 'Show Post twitter share',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_gp',
        'label'       => 'Show Post Google plus',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_fb',
        'label'       => 'Blog facebook',
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_author',
        'label'       => 'Show post author',
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_tags',
        'label'       => 'Show post tags',
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_thumbs',
        'label'       => 'Show thumbs at blog list',
        'desc'        => '',
        'std'         => '',
        'type'        => 'on-off',
        'section'     => 'blog',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),


        /* START: Listings Settings */
//      array(
//        'id'          => 'lstng_filter_preset',
//        'label'       => 'Preset filtering on:',
//        'desc'        => 'After adding some "listings types" type the slug of the type which you want to be used as active filter, leave this blank to ignore it.',
//        'std'         => '',
//        'type'        => 'text',
//        'section'     => 'listings',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//
//      array(
//        'id'          => 'lstng_layout',
//        'label'       => 'Listings items layout',
//        'desc'        => '',
//        'std'         => 'default',
//        'type'        => 'radio',
//        'section'     => 'listings',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and',
//        'choices'     => array(
//          array(
//            'value'       => 'default',
//            'label'       => 'Default',
//            'src'         => ''
//          ),
//          array(
//            'value'       => 'compact',
//            'label'       => 'compact',
//            'src'         => ''
//          )
//        )
//      ),
//      array(
//        'id'          => 'lstng_filter',
//        'label'       => 'Listings filter layout',
//        'desc'        => '',
//        'std'         => 'dropdown',
//        'type'        => 'radio',
//        'section'     => 'listings',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and',
//        'choices'     => array(
//          array(
//            'value'       => 'dropdown',
//            'label'       => 'DropDown',
//            'src'         => ''
//          ),
//          array(
//            'value'       => 'inline',
//            'label'       => 'Inline badges',
//            'src'         => ''
//          )
//        )
//      ),


      array(
        'id'          => 'etc_analytics_code',
        'label'       => 'Analytics script',
        'desc'        => 'Paste your Google analytics ( or other services ) code here',
        'std'         => '',
        'type'        => 'textarea-simple',
        'section'     => 'etc',
        'rows'        => '3',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'etc_fav_icon',
        'label'       => 'Site favicon',
        'desc'        => 'Upload a 16x16 or 32x32 .png or .ico file',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'etc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'copyright',
        'label'       => 'Copyright text',
        'desc'        => '',
        'std'         => 'Copyright text here',
        'type'        => 'text',
        'section'     => 'etc',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
//      array(
//        'id'          => 'no_perfect_scroll',
//        'label'       => 'Use fancy scrollbars?',
//        'desc'        => '',
//        'std'         => 'on',
//        'type'        => 'on-off',
//        'section'     => 'etc',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      ),
//      array(
//        'id'          => 'circle_around_logo',
//        'label'       => 'Circled profile picture?',
//        'desc'        => 'default is on which means it will wrap your image with a circle.',
//        'std'         => 'on',
//        'type'        => 'on-off',
//        'section'     => 'etc',
//        'rows'        => '',
//        'post_type'   => '',
//        'taxonomy'    => '',
//        'min_max_step'=> '',
//        'class'       => '',
//        'condition'   => '',
//        'operator'    => 'and'
//      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( 'option_tree_settings_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}