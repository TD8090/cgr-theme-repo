<?php
/**
 * Created by PhpStorm.
 * User: Brak
 * Date: 3/16/2015
 * Time: 6:25 PM
 */


//custom widget tutorial /*8-19-2013*/
class Listings_Widget extends WP_Widget
{
    function __construct() {
        parent::__construct(
            'listings_widget', // Base ID
            'Listings Widget', // Name
            array('description' => __( 'Displays latest listings with thumbnail, title and date'))
        );
    }
    function widget($args, $instance) { //output
        extract( $args );
        // these are the widget options
        $title = apply_filters('widget_title', $instance['title']);
        $numberOfListings = $instance['numberOfListings'];
        echo $before_widget;
        // Check if title is set
        if ( $title ) {
            echo $before_title . $title . $after_title;
        }
        $this->getRealtyListings($numberOfListings);
        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['numberOfListings'] = strip_tags($new_instance['numberOfListings']);
        return $instance;
    }

    // widget form creation
    function form($instance) {

        // Check values
        if( $instance) {
            $title = esc_attr($instance['title']);
            $numberOfListings = esc_attr($instance['numberOfListings']);
        } else {
            $title = '';
            $numberOfListings = '';
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'listings_widget'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('numberOfListings'); ?>"><?php _e('Number of Listings:', 'listings_widget'); ?></label>
            <select id="<?php echo $this->get_field_id('numberOfListings'); ?>"  name="<?php echo $this->get_field_name('numberOfListings'); ?>">
                <?php for($x=1;$x<=10;$x++): ?>
                    <option <?php echo $x == $numberOfListings ? 'selected="selected"' : '';?> value="<?php echo $x;?>"><?php echo $x; ?></option>
                <?php endfor;?>
            </select>
        </p>
    <?php
    }

/*$meta= get_post_custom($post->ID);*/


    function getRealtyListings($numberOfListings) { //html
        global $post;
        add_image_size( 'listings_widget_size', 45, 45, false );
        $listings = new WP_Query();
        $listings->query('post_type=listings&posts_per_page=' . $numberOfListings );
        if($listings->found_posts > 0) {
            echo '<ul class="listings_widget">';
            while ($listings->have_posts()) {
                $listings->the_post();
                $image = (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail($post->ID, 'listings_widget_size') : '<div class="noThumb"></div>';
                $listItem = '<li>' . $image;
                $listItem .= '<a href="' . get_permalink() . '">';
                $listItem .= get_the_title() . '</a>';
                $listItem .= '<span>Added ' . get_the_date() . '</span>';
/*                $listItem .= '<span>' . cgr_taxonomy_name('name') . '</span></li>';*/
                $listItem .= '<span>' . get_the_excerpt() . '</span></li>';
                echo $listItem;
            }
            echo '</ul>';
            wp_reset_postdata();
        }else{
            echo '<p style="padding:25px;">No listing found</p>';
        }
    }

} //end class Listings_Widget
register_widget('Listings_Widget');









/////////////////////////////


