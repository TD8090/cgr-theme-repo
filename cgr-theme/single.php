

<?php get_header(); ?>

	<div id="single-php" class="cgr-page home" data-pos="home" data-url="<?php the_permalink(); ?>">
		<div id="inside">



		    <div class="page-container" id="ajax-single-post">

				<!-- here will be populated with the single post content -->
				<?php the_post(); ?>
				<div class="pageheadsect">
				    <div class="headsectcontent">
				        <div class="section-container">
				            
				            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
							<?php if($url): ?>
								<img src="<?php echo $url; ?>" alt="" class="img-responsive"/>
							<?php endif; ?>
				            <h2 class="title"><?php the_title(); ?></h2>
				            
				            <div class="post-meta">
				            	
				            	<span><i class="fa fa-calendar"></i>&nbsp;<?php the_date(); ?></span>
					            
					            <?php if (ot_get_option( 'blog_author' ) == 'on') : ?>
					            | <span><i class="fa fa-edit"></i>&nbsp;<?php the_author(); ?></span>
					            <?php endif; ?>

					            | <span><i class="fa fa-folder-o"></i>&nbsp;<?php the_category(', ','single'); ?></span>
					            
					            <?php if (ot_get_option( 'blog_tags' ) == 'on') : ?>
					            | <span><i class="fa fa-tag"></i>&nbsp;<?php the_tags(''); ?></span>
					            <?php endif; ?>
					            
					           
					            
					            <?php if (ot_get_option('blog_fb') == "on"): ?>
								    <div id="fb-root"></div>
								    <?php if (function_exists('cgr_add_facebook')) : ?>
								    	<?php cgr_add_facebook(); ?>
								    	| <div class="fb-share-button" data-href="<?php the_permalink();?>" data-width="30px" data-type="button_count"></div>
								    <?php endif; ?>
								<?php endif; ?>

								<!-- twitter -->
								<?php if(ot_get_option( 'blog_twitter' ) == 'on'): ?>
									| 
									<div class="twittershare">
										<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php the_permalink();?>">Tweet</a>
										<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
									</div>
								<?php endif; ?>
								<!-- /twitter	 -->

								<?php if(ot_get_option( 'blog_gp' ) == 'on'): ?>
									| 
									<div class="googleplusone">
										<!-- Place this tag where you want the +1 button to render. -->
										<div class="g-plusone" data-size="medium" data-annotation="inline" data-width="120"></div>

										<!-- Place this tag after the last +1 button tag. -->
										<script type="text/javascript">
										  (function() {
										    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
										    po.src = 'https://apis.google.com/js/platform.js';
										    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
										  })();
										</script>
									</div>
									
					            <?php endif; ?>


				            </div>
				            
				        </div>
				    </div>
				</div>

				<div class="page-contents color-1">
					<div class="section">
						<div class="section-container">
							<?php the_content(); ?>
							<?php wp_link_pages(); ?>
						</div>
					</div>
				</div>



			</div>


			</div>

		</div>
	</div>

	<?php get_footer(); ?>



