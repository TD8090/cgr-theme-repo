<?php
/**
 * @package WordPress
 * @subpackage cgr-theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	<title>
		<?php
		global $page, $paged;
		wp_title( '|', true, 'right' );
		bloginfo( 'name' );
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'themename' ), max( $paged, $page ) );
		?>
	</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
    <?php wp_head(); ?>
    <link rel="stylesheet" type="text/css" media="all"
          href="<?php bloginfo('template_directory'); ?>/public/css/less.compiled.min.css" />
</head>
	
	<body  <?php body_class(); ?>>
	
        <div id="wrapper">

<!-- BEGIN: Header Section -->
<div id="headsect">

    <div id="topbar">
        <div class="contact-icons">
            <ul>
                <?php if (ot_get_option( 'topbar_phone_tog' )=="on"): ?>
                    <li><a href="tel:<?php echo ot_get_option( 'topbar_phone_number' ); ?>"><i class="fa fa-phone"></i>
                        </a><?php echo ot_get_option( 'topbar_phone_number' ); ?></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_email_tog' )=="on"): ?>
                    <li><a href="mailto:<?php echo ot_get_option( 'topbar_email_address' ); ?>"><i class="fa fa-envelope-o"></i>
                        </a><?php echo ot_get_option( 'topbar_email_address' ); ?></li>
                <?php endif; ?>
            </ul>
        </div>

        <div class="social-icons">
            <ul>
                <?php if (ot_get_option( 'topbar_facebook_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_facebook_url' ); ?>" target="blank"><i class="fa fa-facebook"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_twitter_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_twitter_url' ); ?>" target="blank"><i class="fa fa-twitter"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_gplus_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_gplus_url' ); ?>" target="blank"><i class="fa fa-google-plus"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_linkedin_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_linkedin_url' ); ?>" target="blank"><i class="fa fa-linkedin"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_youtube_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_youtube_url' ); ?>" target="blank"><i class="fa fa-youtube"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_instagram_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_instagram_url' ); ?>" target="blank"><i class="fa fa-instagram"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_flickr_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_flickr_url' ); ?>" target="blank"><i class="fa fa-flickr"></i></a></li>
                <?php endif; ?>
                <?php if (ot_get_option( 'topbar_pinterest_tog' )=="on"): ?>
                    <li><a href="<?php echo ot_get_option( 'topbar_pinterest_url' ); ?>" target="blank"><i class="fa fa-pinterest"></i></a></li>
                <?php endif; ?>

            </ul>
        </div>
    </div> <!--END:#topbar-->

    <div id="brand-area" class="clearfix">
        <div id="brand_banner">
            <a href="<?php /*echo get_site_url();*/?>" id="brand_banner_link">
                <div id="div_carolina">
                    <div class="txt_carolina">
                        <p>Carolina Girls</p>
                    </div>
                </div>
                <div id="div_real">
                    <div class="txt_real">
                        <p>Real Estate Group</p>
                    </div>
                </div>
            </a>
        </div>
</div>

    <div id="main-nav">

        <div id="nav-container">


            <?php
            /**
             * Displays a navigation menu
             * @param array $args Arguments
             */
            wp_nav_menu( array(
                'theme_location' => 'headsect',
                'menu' => '',
                'container' => false,
                'menu_class' => false,
                'items_wrap' => '<ul id = "navigation" class = "%2$s">%3$s</ul>',
                'depth' => 0,
                'walker' => ''//new cgr_walker_nav_menu
            ) ); ?>

        </div>
    </div>

    <!-- BEGIN: Mobile Toggle -->
    <!--
        <div class="mobile_nav_trigger">
            <a href="#headsect" class="mobilemenu"><i class="fa fa-bars"></i></a>
        </div>
    -->
        <!-- END: Mobile Toggle -->


</div> <!--END:#headsect-->
<!-- END: Header Section -->

<!-- BEGIN: Main Content - continued in all other templates -->
            <div id="main">