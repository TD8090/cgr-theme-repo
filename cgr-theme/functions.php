<?php
/**
 * @package WordPress
 * @subpackage cgr-theme
 */

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );

/**
 * Optional: set 'ot_show_new_layout' filter to false.
 * This will hide the "New Layout" section on the Theme Options page.
 */
add_filter( 'ot_show_new_layout', '__return_false' );

/*
* Required: set 'ot_theme_mode' filter to true.
*/ 
add_filter( 'ot_theme_mode', '__return_true' );

/**
 * Required: include OptionTree.
 */
load_template( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );

/**
 * Theme Options
 */
load_template( trailingslashit( get_template_directory() ) . 'theme-options.php' );


/**
 * Make theme available for translation
 * Translations can be filed in the /languages/ directory
 */
load_theme_textdomain( 'cgrslug', get_template_directory() . '/languages' );

$locale = get_locale();
$locale_file = get_template_directory() . "/languages/$locale.php";
if ( is_readable( $locale_file ) )
	require_once( $locale_file );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 900;


/**
 * Enqueue the Google fonts.
 */
function cgr_google_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'cgr-lato', "$protocol://fonts.googleapis.com/css?family=Lato:100,300,400,700,100italic,300italic,400italic" );}
add_action( 'wp_enqueue_scripts', 'cgr_google_fonts' );


/**
* Enqueue scripts and styles for the front end.
* @since cgr-theme 1.0
* @return void
*/
add_action('wp_enqueue_scripts', 'cgr_add_javascripts');
function cgr_add_javascripts() {
    // wp_deregister_script( 'jquery' );
    // wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'modernizer', get_template_directory_uri() 		. '/src/js/modernizr.custom.63321.js');
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/src/js/bootstrap.min.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'touchSwip', get_template_directory_uri() 		. '/src/js/jquery.touchSwipe.min.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'mouswheel', get_template_directory_uri() 		. '/src/js/jquery.mousewheel.js', array( 'jquery' ),'',true );
//	wp_enqueue_script( 'carouFredSel', get_template_directory_uri() 	. '/src/js/jquery.carouFredSel-6.2.1-packed.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'dropdown', get_template_directory_uri() 		. '/src/js/jquery.dropdownit.js', array( 'jquery' ),'',true );
//	wp_enqueue_script( 'mixitup', get_template_directory_uri() 			. '/src/js/jquery.mixitup.min.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'touchSwip', get_template_directory_uri() 		. '/src/js/jquery.touchSwipe.min.js', array( 'jquery' ),'',true );
//	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() 	. '/src/js/magnific-popup.js', array( 'jquery' ),'',true );
//	wp_enqueue_script( 'masonry', get_template_directory_uri() 			. '/src/js/masonry.min.js','','',true);
	wp_enqueue_script( 'perfect-scroll', get_template_directory_uri() 	. '/src/js/perfect-scrollbar.min.js', array('jquery'),'',true);
//	wp_enqueue_script( 'scrollTo', get_template_directory_uri() 		. '/src/js/ScrollToPlugin.min.js', array('jquery'),'',true);
	wp_enqueue_script( 'tweenmax', get_template_directory_uri() 		. '/src/js/TweenMax.min.js','','',true);
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() 	. '/src/js/imagesloaded.js','','',true);

//	wp_enqueue_script( 'scroller', get_template_directory_uri() 		. '/src/js/jquery.simplyscroll.js','','',true);

	wp_enqueue_script( 'cgr-script', get_template_directory_uri() 	. '/public/js/custom.js', array( 'jquery' ),'',true );
	wp_enqueue_script( 'comment-reply' );
}    
/* passing in: array('jquery'), above, checks if jQuery has already been included or not*/
/**
* Inject custom script
* @since v1.1.0 
*/
function add_custom_js() {
  
    	$script = 	'var siteUrl = "'.home_url().'";';
	if (is_singular())
	{
		$script .= ' var isSingle = true;';
		$script .= ' var blogUrl = "'.home_url().'/blog/";';
	}

	if ( function_exists( 'ot_get_option' ) ){
		
//		$script .= ' var perfectScroll = "'.ot_get_option('no_perfect_scroll').'";';
		
//		$script .= ' var blogAjaxState = "'.ot_get_option('blog_ajax').'";';
		
//		if (ot_get_option('lstng_filter_preset')=='')
//			$lstngfilter = "false";
//		else
//			$lstngfilter = ot_get_option('lstng_filter_preset');
//		$script .= ' var lstngsFilter = "'.$lstngfilter.'";';
	}

    echo '<script type="text/javascript">'.$script.'</script>';
}
add_action('wp_head', 'add_custom_js');


/**
* Inject custom styles
*/
function cgr_custom_css() {
  
   	if ( function_exists( 'ot_get_option' ) ){
   		$styles='';

		if (ot_get_option('brand_logo')=='off'){
   			$styles .= '
			.mobilemenu {
				width: 150px;
			}
			';
   		}
   	}

}
add_action('wp_head', 'cgr_custom_css');



/**
* Inject analytics
* @since v1.1.0 
*/
function cgr_analytics() {
	if ( function_exists( 'ot_get_option' ) ){
		echo ot_get_option( 'etc_analytics_code' );
	} 
}
add_action('wp_head', 'cgr_analytics');

/**
* Inject favicon
* @since v1.1.0 
*/
function cgr_favicon() {
	if ( function_exists( 'ot_get_option' ) ){
		$favicon = '<link rel="icon" type="image/png" href="'. ot_get_option('etc_fav_icon').'">';
		echo $favicon;
	} 
}
add_action('wp_head', 'cgr_favicon');

/**
*	Facebook script
* @since v 1.1.0
*/
function cgr_add_facebook(){
	if ( function_exists('ot_get_option')){
		if (ot_get_option('blog_fb') == "on") {
			$script = '
				<script>(function(d, s, id) {
			  	var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, "script", "facebook-jssdk"));
			</script>
			';
			echo $script;	
		}
	}
}
//add_action('wp_head','cgr_add_facebook' );


/** 
* IE fixes
* @since v 1.1.0
*/
function cgr_inject_ie(){

	echo '
	<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	';
}
add_action( 'wp_head','cgr_inject_ie' );




/**
* Enqueue styles for the front end.
* @since cgr-theme 1.0
* @return void
*/
add_action( 'wp_enqueue_scripts', 'cgr_add_styles' );

function cgr_add_styles() {

//	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/src/css/bootstrap.css', array() );
//	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array() );
//	wp_enqueue_style( 'magnific-pupup', get_template_directory_uri() . '/src/css/magnific-popup.css', array() );
//	wp_enqueue_style( 'perfect-scroll-style', get_template_directory_uri() . '/src/css/perfect-scrollbar-0.4.5.min.css', array() );
//	wp_enqueue_style( 'simscroll-style', get_template_directory_uri() . '/src/css/jquery.simplyscroll.css', array() );

	// Add +cgr specific
//	wp_enqueue_style( 'cgr-styles', get_template_directory_uri() . '/css/style.css', array('bootstrap-style') );

    //main stylesheet - moved to header.php
//!!wp_enqueue_style( 'cgr-styles', get_template_directory_uri() . '/css/less.compiled.min.css', array() );

	// more cgr specific
/*	wp_enqueue_style( 'cgr-custom-style', get_template_directory_uri() . '/css/styles/default.css', array('bootstrap-style') );
*/
}



/**
 * Remove code from the <head>
 */
//remove_action('wp_head', 'rsd_link'); // Might be necessary if you or other people on this site use remote editors.
//remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
//remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
//remove_action('wp_head', 'index_rel_link'); // Displays relations link for site index
//remove_action('wp_head', 'wlwmanifest_link'); // Might be necessary if you or other people on this site use Windows Live Writer.
//remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
//remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
//remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); // Display relational links for the posts adjacent to the current post.



// Hide the version of WordPress you're running from source and RSS feed 
// Want to JUST remove it from the source? Try: remove_action('wp_head', 'wp_generator');
function hcwp_remove_version() {return '';}
add_filter('the_generator', 'hcwp_remove_version');



/**
 * Use wp_nav_menus() for the header
 */
if (function_exists('register_nav_menu')) {
	register_nav_menu( 'headsect', 'Main Menu' );
}

/** 
 * Add default posts and comments RSS feed links to head
 */
if ( function_exists( 'add_theme_support')){
	add_theme_support( 'automatic-feed-links' );
}


/** 
 * Use post thumbnails
 */
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'admin-gallery-thumb', 80, 80, true); //admin thumbnail
}


/** 
 * Add default posts and comments RSS feed links to head
 */
if ( function_exists( 'add_theme_support')){
    add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 70, 70, true );
}


/*
* Include custom post types (CPTs)
*/
require_once( get_template_directory().'/type-listings.php');
require_once( get_template_directory().'/type-listings-meta-box.php');

/*
* Include custom widget: CPT-listings "Recent Listings"
*/
require_once( get_template_directory().'/src/widget-recentlistings.php');



/*
* Use custom excerpt length
*/
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/* FROM http://wordpress.stackexchange.com/a/30854
 * Call archive-
*/
/*add_action("pre_get_posts", "custom_front_page");
function custom_front_page($wp_query){
    //Ensure this filter isn't applied to the admin area
    if(is_admin()) {    return;    }

    if($wp_query->get('page_id') == get_option('page_on_front')):
        $wp_query->set('post_type', 'listings');
        $wp_query->set('page_id', ''); //Empty
        //Set properties that describe the page to reflect that
        //we aren't really displaying a static page
        $wp_query->is_page = 0;
        $wp_query->is_singular = 0;
        $wp_query->is_post_type_archive = 1;
        $wp_query->is_archive = 1;
    endif;
}*/


?>