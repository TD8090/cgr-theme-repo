<?php 
add_action('init', 'listings_register');

function listings_register(){
	$listings_labels = array(
	    'name' 					=> __('Listings','cgrslug'),
	    'singular_name' 		=> __('Listings','cgrslug'),
	    'add_new' 				=> __('Add New','cgrslug'),
	    'add_new_item' 			=> __("Add New Listings",'cgrslug'),
	    'edit_item' 			=> __("Edit Listings",'cgrslug'),
	    'new_item' 				=> __("New Listings",'cgrslug'),
	    'view_item' 			=> __("View Listings",'cgrslug'),
	    'search_items' 			=> __("Search Listings",'cgrslug'),
	    'not_found' 			=> __('No Listings found','cgrslug'),
	    'not_found_in_trash' 	=> __('No Listings found in Trash','cgrslug'),
	    'parent_item_colon' 	=> ''
	);
	$listings_args = array(
	    'labels' 				=> $listings_labels ,
	    'public' 				=> true,
	    'publicly_queryable' 	=> true,
	    'show_ui' 				=> true, 
	    'query_var' 			=> true,
	    'rewrite' 				=> true,
	    'hierarchical' 			=> false,
	    'menu_position' 		=> null,
	    'capability_type' 		=> 'post',
	    'supports' 				=> array('title', /*'editor',*/ 'excerpt', 'thumbnail'),
	    'menu_icon' 			=> get_bloginfo('template_directory') . '/src/img/listings-icon.png' //16x16 png if you want an icon
	); 
	register_post_type('listings', $listings_args);
	flush_rewrite_rules( false );
};


add_action( 'init', 'cgr_create_listings_taxonomies', 0);
 
function cgr_create_listings_taxonomies(){
    register_taxonomy(
        'lstngtype', 'listings',
        array(
            'hierarchical'=> true, 
            'label' => 'Listings Types',
            'singular_label' => 'Listings Type',
            'rewrite' => true
        )
    );    
}


add_action('manage_posts_custom_column', 'cgr_custom_columns');
add_filter('manage_edit-listings_columns', 'cgr_add_new_listings_columns');
function cgr_add_new_listings_columns( $columns ){
    $columns = array(
        'cb'		=> '<input type="checkbox">',
		'title'		=> 'Listings Title',
		'lstngtype'	=> 'Listings Type',
		'date'		=> 'Date'
    );
    return $columns;
}


function cgr_custom_columns( $column ){
    global $post;
    
    switch ($column) {
        case 'lstngtype' : echo get_the_term_list( $post->ID, 'lstngtype', '', ', ',''); break;
    }
}


function cgr_get_lstngtypes(){
	$taxonomy = 'lstngtype';
	return get_terms($taxonomy);
}

function cgr_taxonomy_name($type){
     global $post;
 
    $terms = get_the_terms( $post->ID , 'lstngtype' );
    if ($type =="slug"){
	    foreach ( $terms as $termphoto ) { 
	        echo ' '.$termphoto->slug; 
	    }
	}else{
		foreach ( $terms as $termphoto ) { 
	        echo '<span class="label label-default">'.$termphoto->name.'</span>';
	    }
	} 
}


