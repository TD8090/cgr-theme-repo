<?php
/*
Template Name: Home Template
*/
?>

<?php get_header(); ?>
<div id="page-listings" class="cgr-page home">
    <?php the_post(); ?>
    <div id="inside">
        <div id="listings" class="page">
            <div class="page-container">
                <div class="pageheadsect">
                    <div class="headsectcontent">
                        <div class="section-container">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pagecontents">


                    <div class="section color-2" id="lstng-grid">
                        <div class="section-container">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wkitems">
                                        <?php
                                        //setup new WP_Query
                                        $wp_query = new WP_Query( 
                                            array(
                                                'posts_per_page'    =>    -1,
                                                'post_type'         =>    'listings'
                                            )
                                        );
                                        $counter = 0;
                                        //begin loop
                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        $meta= get_post_custom($post->ID);

                                        ?>


<div class="item <?php cgr_taxonomy_name('slug'); ?>"

<?php if (array_key_exists('cgr_lstng_year', $meta)):  ?>
    data-year="<?php echo $meta['cgr_lstng_year'][0] ?>"
<?php endif; ?>   >

    <div class="listingmain">
        <div class="listingassets">

            <?php if ($post->post_content!="") : ?>
            <a href="#" class="listingcollapse">
                <i class="fa fa-plus-square-o"></i>
            </a>
            <?php endif; ?>

            <?php if (array_key_exists('cgr_lstng_ext_link', $meta)):  ?>
            <a href="<?php echo $meta['cgr_lstng_ext_link'][0] ?>" class="tooltips" title="External link" target="_blank">
                <i class="fa fa-external-link"></i>
            </a>
            <?php endif; ?>

            <?php if(array_key_exists('cgr_lstng_docfile', $meta)): ?>
            <a href="<?php echo $meta['cgr_lstng_docfile'][0] ?>" class="tooltips" title="<?php _e( 'Download', 'cgrslug' ); ?>" target="_blank">
                <i class="fa fa-cloud-download"></i>
            </a>
            <?php endif; ?>

        </div>

        <h3 class="listingtitle"><?php echo $meta['cgr_lstng_title'][0] ?></h3>

        <?php cgr_taxonomy_name('name'); ?>

        <div class="lstngprice"><strong>Price:</strong> <?php echo $meta['cgr_lstng_price'][0] ?></div>


   
            <?php if (array_key_exists('cgr_lstng_tagline', $meta)):  ?>
        <div class="lstngtagln"><strong>"<?php echo $meta['cgr_lstng_tagline'][0] ?>"</strong></div>
            <?php endif; ?>
			
            <?php if (array_key_exists('cgr_lstng_year', $meta)):  ?>
        <div class="lstngcite"><strong>Updated:</strong> <?php echo $meta['cgr_lstng_year'][0] ?></div>
            <?php endif; ?>
			
        <div class="assoc_tech"><strong>Tech:</strong> <?php echo $meta['cgr_lstng_assoc_tech'][0] ?></div>

            <?php if ( has_post_thumbnail() ) { ?>
        <div class="lstngthumb">
            <?php the_post_thumbnail(); ?>
        </div>
            <?php } ?>

    </div>
    <?php if ($post->post_content!="") : ?>
    <div class="listingdetails">
        <?php the_content(); ?>
    </div>
    <?php endif; ?>
</div>

                                        <?php endwhile; // end of the loop. ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php get_footer(); ?>


