(function($, window, document, undefined){
	

	$.fn.exists = function () {
	    return this.length > 0 ? this : false;
	}


	//Handle window resize
/*
	var resizeFlag=0;
	$(window).resize(function(){
	    resizeFlag=1;
	})
	checkSizeChange();
	function checkSizeChange(){
	    if (resizeFlag) {
	      resizeFlag=0;
	      $(window).trigger('resized');
	    }
	    setTimeout(function(){
	      checkSizeChange();
	    }, 200);
	}

*/


	$(window).on('newContent',function(){
		initiate();
	});

	$(document).ready(function(){
		initiate();
	});

	function initiate(){
		//tooltips
		$(".tooltips").tooltip();

	}




	/*++++++++++++++++++++++++++++++++++++++++++++++
		custom scrolls with perfectScroll plugin
	++++++++++++++++++++++++++++++++++++++++++++++++*/
/*
	perfectScrollIt();
	$(window).on('newContent',function(){
		perfectScrollIt();		
	});

	function perfectScrollIt(){
		var $cgr_page=$('body');

        $cgr_page.perfectScrollbar({
            wheelPropagation:true,
            wheelSpeed:80
        });

	}
*/





	/*++++++++++++++++++++++++++++++++++++
		gallery overlays and popups
	++++++++++++++++++++++++++++++++++++++*/
/*
	$("body").on("mouseenter",".grid li",function(){
		new TweenLite($(this).find(".over"),0.4,{bottom:0,top:0});
	}).on("mouseleave",".grid li",function(){
		new TweenLite($(this).find(".over"),0.4,{bottom:"-100%", top:"100%"});
	});
*/



})(jQuery, window, document);

