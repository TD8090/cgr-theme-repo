<?php
/*
Template Name: Listings Template
*/
?>

<?php get_header(); ?>
<div id="page-listings" class="cgr-page home">
    <?php the_post(); ?>
    <div id="inside">
        <div id="listings" class="page">
            <div class="page-container">

                <div class="row clearfix">
                    <div class="col-md-12 column">
                        <div class="page-header">
                            <h1>
                                Example page header <small>Subtext for header</small>
                            </h1>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12 column">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-8 column">
                                <div class="panel-group" id="panel-848793">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title" data-toggle="collapse" data-parent="#panel-848793" href="#panel-element-114350">Collapsible Group Item #1</a>
                                        </div>
                                        <div id="panel-element-114350" class="panel-collapse in">
                                            <div class="panel-body">
                                                Anim pariatur cliche...
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-848793" href="#panel-element-997474">Collapsible Group Item #2</a>
                                        </div>
                                        <div id="panel-element-997474" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                Anim pariatur cliche...<br/>Anim pariatur cliche...<br/>Anim pariatur cliche...<br/>Anim pariatur cliche...<br/>Anim pariatur cliche...<br/>Anim pariatur cliche...<br/>Anim pariatur cliche...<br/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-12 column">
                                        <div class="wkitems">
                                            <?php
                                            //setup new WP_Query
                                            $wp_query = new WP_Query(
                                                array(
                                                    'posts_per_page'    =>    -1,
                                                    'post_type'         =>    'listings'
                                                )
                                            );
                                            $counter = 0;
                                            //begin loop
                                            while ($wp_query->have_posts()) : $wp_query->the_post();

                                                $meta= get_post_custom($post->ID);

                                                ?>


                                                <div class="item <?php cgr_taxonomy_name('slug'); ?>"

                                                    <?php if (array_key_exists('cgr_lstng_year', $meta)):  ?>
                                                        data-year="<?php echo $meta['cgr_lstng_year'][0] ?>"
                                                    <?php endif; ?>   >

                                                    <div class="listingmain">
                                                        <div class="listingassets">
                                                            <a href="<?php the_permalink(); ?>" class="listingcollapse">
                                                                <i class="fa fa-arrow-right "></i>
                                                            </a>
                                                        </div>

                                                        <h3 class="listingtitle"><?php echo $meta['cgr_lstng_title'][0] ?></h3>

                                                        <?php cgr_taxonomy_name('name'); ?>

                                                        <div class="lstngprice"><strong>Price:</strong> <?php echo $meta['cgr_lstng_price'][0] ?></div>



                                                        <?php if (array_key_exists('cgr_lstng_tagline', $meta)):  ?>
                                                            <div class="lstngtagln"><strong>"<?php echo $meta['cgr_lstng_tagline'][0] ?>"</strong></div>
                                                        <?php endif; ?>


                                                        <?php if ( has_post_thumbnail() ) { ?>
                                                            <div class="lstngthumb">
                                                                <?php the_post_thumbnail(); ?>
                                                            </div>
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                            <?php endwhile; // end of the loop. ?>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 column">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Panel title
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>Panel content<br/>
                                    </div>
                                    <div class="panel-footer">
                                        Panel footer
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pageheadsect">
                    <div class="headsectcontent">
                        <div class="section-container">
                            <h2 class="title"><?php the_title(); ?></h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pagecontents">


                    <div class="section color-2" id="lstng-grid">
                        <div class="section-container">
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wkitems">
                                        <?php
                                        //setup new WP_Query
                                        $wp_query = new WP_Query( 
                                            array(
                                                'posts_per_page'    =>    -1,
                                                'post_type'         =>    'listings'
                                            )
                                        );
                                        $counter = 0;
                                        //begin loop
                                        while ($wp_query->have_posts()) : $wp_query->the_post();

                                        $meta= get_post_custom($post->ID);

                                        ?>


<div class="item <?php cgr_taxonomy_name('slug'); ?>"

<?php if (array_key_exists('cgr_lstng_year', $meta)):  ?>
    data-year="<?php echo $meta['cgr_lstng_year'][0] ?>"
<?php endif; ?>   >

    <div class="listingmain">
        <div class="listingassets">
            <a href="<?php the_permalink(); ?>" class="listingcollapse">
                <i class="fa fa-arrow-right "></i>
            </a>
        </div>

        <h3 class="listingtitle"><?php echo $meta['cgr_lstng_title'][0] ?></h3>

        <?php cgr_taxonomy_name('name'); ?>

        <div class="lstngprice"><strong>Price:</strong> <?php echo $meta['cgr_lstng_price'][0] ?></div>


   
            <?php if (array_key_exists('cgr_lstng_tagline', $meta)):  ?>
        <div class="lstngtagln"><strong>"<?php echo $meta['cgr_lstng_tagline'][0] ?>"</strong></div>
            <?php endif; ?>
			

            <?php if ( has_post_thumbnail() ) { ?>
        <div class="lstngthumb">
            <?php the_post_thumbnail(); ?>
        </div>
            <?php } ?>

    </div>
</div>
<?php endwhile; // end of the loop. ?>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php get_footer(); ?>


